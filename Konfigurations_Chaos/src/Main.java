
public class Main {

	//wCmM6EKtN6zByk85YUc5
	public static void main(String[] args) {
		
		final byte PRUEFNR = 4;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		double maximum = 100.0;
		double patrone = 46.24;
		double fuellstand = maximum - patrone;
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		char sprachModul = 'd';
		boolean statusCheck;
		String name;
		int euro;
		int cent;
		int summe;
		
		name = typ + " " + bezeichnung;
		summe = muenzenCent + muenzenEuro * 100;
		cent = summe % 100;
		euro = summe / 100;
		statusCheck = (euro <= 150) && (euro >= 50) && (cent != 0) && (sprachModul == 'd') && (fuellstand >= 50.00) && (!(PRUEFNR == 5 || PRUEFNR == 6));
		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer: " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + fuellstand);
		System.out.println("Summe Euro: " + euro + " Euro");
		System.out.println("Summe Rest: " + cent + " Cent");
		System.out.println("Status: " + statusCheck);

	}

}
