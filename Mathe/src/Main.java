
public class Main {
	
	//wCmM6EKtN6zByk85YUc5

	public static void main(String[] args) {
		double h = hypotenuse(2, 6);
		System.out.println(h);

	}
	
	public static double quadrat(double x) {
		return Math.pow(x, 2);
	}
	
	public static double hypotenuse(double kathete1, double kathete2) {
		return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
	}

}
