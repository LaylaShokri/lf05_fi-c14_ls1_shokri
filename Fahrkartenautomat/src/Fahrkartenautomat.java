import java.util.Scanner;

@SuppressWarnings("ALL")
class Fahrkartenautomat {

    //wCmM6EKtN6zByk85YUc5
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double zwischensumme = 0;
        boolean selectMore = true;
        final boolean RERUN = true;

        while (RERUN) {
            while (selectMore) {
                int selectedTicket = selectTicket(scan);

                switch (selectedTicket) {
                    case 1:
                    case 2:
                    case 3: {
                        int anzahlTickets = anzahlTicketsErfassen(scan);
                        zwischensumme += calculateZuZahlenderBetrag(anzahlTickets, selectedTicket);
                        System.out.printf("Zwischensumme: %.2f€\n\n", zwischensumme);
                        break;
                    }
                    case 9: {
                        double eingezahlterGesamtbetrag = fahrkartenBezahlen(scan, zwischensumme);
                        fahrkarteAusgeben();
                        rueckgeldBerechnung(eingezahlterGesamtbetrag, zwischensumme);
                        break;
                    }
                    default:
                        System.out.println("Falsche Eingabe.");
                }
            }
        }
    }


    public static int anzahlTicketsErfassen(Scanner scan) {
        System.out.print("Anzahl der Tickets: ");
        int anzahlTickets = scan.nextInt();
        System.out.println();

        if (anzahlTickets < 0) {
            System.out.println("Negative Anzahl an Tickets nicht erlaubt.");
            anzahlTickets = 1;
        } else if (anzahlTickets > 10) {
            System.out.println("Zu viele Tickets. Anzahl wird auf 1 gesetzt.");
            anzahlTickets = 1;
        }

        return anzahlTickets;
    }

    public static int selectTicket(Scanner scan) {

        int selectedTicket = 0;

        do {
            System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
            System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
            System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
            System.out.println("Bezahlen (9)\n");

            System.out.print("Auswahl: ");
            selectedTicket = scan.nextInt();
        } while (selectedTicket <= 0 || selectedTicket > 3);

        return selectedTicket;
    }

    public static double calculateZuZahlenderBetrag(int anzahlTickets, int selectedTicket) {
        if (selectedTicket == 1) {
            return 2.9 * anzahlTickets;
        } else if (selectedTicket == 2) {
            return 8.6 * anzahlTickets;
        } else if (selectedTicket == 3) {
            return 23.5 * anzahlTickets;
        }

        return 0;
    }


    public static double fahrkartenBezahlen(Scanner scan, double zuZahlenderBetrag) {

        double eingezahlterGesamtbetrag = 0;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f€\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2€): ");
            double eingeworfeneMünze = scan.nextDouble();

            if (eingeworfeneMünze < 0.05 || eingeworfeneMünze > 2) {
                System.out.println("Ungültiger Münzeinwurf.");
            } else {
                eingezahlterGesamtbetrag += eingeworfeneMünze;
            }
        }

        return eingezahlterGesamtbetrag;
    }

    public static void fahrkarteAusgeben() {

        System.out.println("\nFahrschein wird ausgegeben");
        warte(250);
        System.out.println("\n\n");
    }

    public static void rueckgeldBerechnung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

        double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f€ ", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "Euro");
                rueckgabebetrag -= 2;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "Euro");
                rueckgabebetrag -= 1;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "Cent");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "Cent");
                rueckgabebetrag -= 0.20;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "Cent");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "Cent");
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wuenschen Ihnen eine gute Fahrt.");
    }

    public static void warte(int millisekunden) {
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(millisekunden);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.printf("%d %s", betrag, einheit);
    }

}