import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Name: ");
		String name = scan.nextLine();
		
		System.out.println("Alter: ");
		int alter = scan.nextInt();
		
		System.out.println("Hallo " + name + ", du bist " + alter + " Jahre alt.");
	}

}
