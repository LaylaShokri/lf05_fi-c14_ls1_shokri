import java.util.Scanner;

public class Main {
	
	//wCmM6EKtN6zByk85YUc5

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		double v = 0;

		System.out.println("Geschwindigkeit: " + v);
		
		System.out.print("Beschleunigung: ");
		double dv = scan.nextDouble();
		
		v = beschleunige(v, dv);
		System.out.println("Geschwindigkeit nach Beschleunigung: " + v);

	}
	
	public static double beschleunige(double v, double dv) {
		
		double geschwindigkeit = v + dv;
		
		if(geschwindigkeit >= 130) {
			System.out.println("Höchstgeschwindigkeit von 130 km/h erreicht/überschritten");
			return 130;
		}
		else {
			return geschwindigkeit;
		}
		
	}

}
