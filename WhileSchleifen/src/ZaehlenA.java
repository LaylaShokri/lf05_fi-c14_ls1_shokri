import java.util.Scanner;

public class ZaehlenA {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("n: ");
        int n = scan.nextInt();
        int counter = 1;

        // a)
        while(counter <= n) {
            System.out.println(counter);
            counter++;
        }

    }

}
