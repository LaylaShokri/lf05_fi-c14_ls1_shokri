import java.util.Scanner;

public class ZaehlenB {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("n: ");
        int n = scan.nextInt();
        int counter = 1;

        // a)
        while(n >= counter) {
            System.out.println(n);
            n--;
        }

    }

}
