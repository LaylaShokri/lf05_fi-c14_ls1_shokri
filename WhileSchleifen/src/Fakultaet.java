import java.util.Scanner;

public class Fakultaet {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("n: ");
        int n = scan.nextInt();
        int c = 1;
        int result = 1;

        while(n >= c) {
            result *= n;
            n--;
        }

        System.out.println(result);
    }

}
