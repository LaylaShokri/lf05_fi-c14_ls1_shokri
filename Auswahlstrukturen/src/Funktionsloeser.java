import java.util.Scanner;

public class Funktionsloeser {

	//wCmM6EKtN6zByk85YUc5
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("x: ");
		double x = scan.nextDouble();
		double ergebnis;
		final double e = 2.718;
		
		if(x < 0) {
			ergebnis = Math.pow(e, x);
		}
		else if(x > 0 && x <= 3) {
			ergebnis = Math.pow(x, 2) + 1;
		}
		else {
			ergebnis = 2*x+4;
		}
		
		System.out.println(ergebnis);

	}

}
