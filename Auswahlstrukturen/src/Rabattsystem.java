import java.util.Scanner;

public class Rabattsystem {

	//wCmM6EKtN6zByk85YUc5
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Bestellwert?");
		double bestellwert = scan.nextDouble();
		double rabatt = 1;
		final double MWST = 0.19;
		
		if(bestellwert > 0 && bestellwert <= 100) {
			rabatt = 0.1;
		}
		else if(bestellwert > 100 && bestellwert <= 500) {
			rabatt = 0.15;
		}
		else {
			rabatt = 0.2;
		}
		
		bestellwert = bestellwert - (bestellwert * rabatt) + (bestellwert * MWST);

		System.out.println(bestellwert);
	}

}
