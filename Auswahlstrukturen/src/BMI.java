import java.util.Scanner;

public class BMI {

	//wCmM6EKtN6zByk85YUc5
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		double weight;
		double height;
		char gender;
		String classification = "";

		System.out.print("Gewicht: ");
		weight = scan.nextDouble();
		
		System.out.print("Groesse in m: ");
		height = scan.nextDouble();
		
		System.out.print("Geschlecht (m/f): ");
		gender = scan.next().charAt(0);
		
		double bmi = weight / Math.pow(height, 2);
		
		if(gender == 'm') {
			if(bmi < 20) {
				classification = "Untergewicht";
			}
			else if(bmi >= 20 && bmi <= 25) {
				classification = "Normalgewicht";
			}
			else {
				classification = "Uebergewicht";
			}
		}
		
		else if(gender=='f') {
			if(bmi < 19) {
				classification = "Untergewicht";
			}
			else if(bmi >= 19 && bmi <= 24) {
				classification = "Normalgewicht";
			}
			else {
				classification = "Uebergewicht";
			}
		}
		
		else {
			System.out.println("Invalid input.");
			System.exit(0);
		}
		
		System.out.printf("BMI: %.2f \n", bmi);
		System.out.println("Klassifizierung: " + classification);
		
		
	}

}
