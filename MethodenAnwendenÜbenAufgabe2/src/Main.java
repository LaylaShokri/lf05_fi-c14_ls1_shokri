import java.util.Scanner;

public class Main {
	
	//wCmM6EKtN6zByk85YUc5

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?", scan);

		int anzahl = liesInt("Geben Sie die Anzahl ein:", scan);

		double preis = liesDouble("Geben Sie den Nettopreis ein:", scan);

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", scan);
		
		

		// Verarbeiten
		double nettogesamtpreis = berechneNettogesamtpreis(anzahl, preis);
		double bruttogesamtpreis = berechneBruttogesamtpreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
	
	public static String liesString(String text, Scanner scan) {
		System.out.println(text);
		return scan.nextLine();
		
	}
	
	public static int liesInt(String text, Scanner scan) {
		System.out.println(text);
		return scan.nextInt();
		
	}
	
	public static double liesDouble(String text, Scanner scan) {
		System.out.println(text);
		return scan.nextDouble();
		
	}
	
	public static double berechneNettogesamtpreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneBruttogesamtpreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
