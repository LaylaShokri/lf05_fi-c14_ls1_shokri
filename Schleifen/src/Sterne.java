import java.util.Scanner;

public class Sterne {
    
    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Anzahl der Sterne (Höhe): ");
        int sterneInput = scan.nextInt();
        char stern = '*';

        for (int i = 1; i <= sterneInput; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print(stern);
            }
            System.out.println();
        }
        
    }
    
}
