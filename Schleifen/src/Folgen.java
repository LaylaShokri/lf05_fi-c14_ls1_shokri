public class Folgen {

    //wCmM6EKtN6zByk85YUc5
    
    public static void main(String[] args) {
        
        // a)
        System.out.println("a)\n");
        for (int i = 99; i >= 9 ; i-=3) {
            System.out.println(i);
        }

        System.out.println("\n---------------------------\n");

        // b)
        System.out.println("b)\n");
        int nextOdd = 1;

        for (int i = 1; i <= 400; i+=nextOdd) {
            System.out.println(i);
            nextOdd += 2;
        }

        System.out.println("\n---------------------------\n");

        // c)
        System.out.println("c)\n");
        for (int i = 2; i <= 102; i+=4) {
            System.out.println(i);
        }

        System.out.println("\n---------------------------\n");

        // d)
        System.out.println("d)\n");
        for (int i = 4; i <= 1024; i*=2) {
            System.out.println(i);
        }

        System.out.println("\n---------------------------\n");

        //e)
        System.out.println("e)\n");
        for (int i = 2; i <= 32768; i = i * 2) {
            System.out.println(i);
        }

    }
    
}
