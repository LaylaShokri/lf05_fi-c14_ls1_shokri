public class Primzahlen {

    public static void main(String[] args) {

        boolean prime = false;

        for (int i = 1; i <= 100; i++) {
            if(i == 2) {
                System.out.println(i);
            }
            for (int j = 2; j < i; j++) {
                if(i % j == 0) {
                    prime = false;
                    break;
                }
                else {
                    prime = true;
                }

            }
            if(prime) {
                System.out.println(i);
            }
        }
    }

}
