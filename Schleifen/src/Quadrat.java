import java.util.Scanner;

public class Quadrat {

    public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

        System.out.print("Seitenlänge: ");
        int l = scan.nextInt();
        char stern = '*';

        for (int i = 1; i <= l; i++) {
            for (int j = 1; j <= l; j++) {
                if(i == 1 || i == l || j == 1 || j == l) {
                    System.out.print(stern + "\t");
                }
                else {
                    System.out.print("\t");
                }
            }
            System.out.println();
        }
    }

}
