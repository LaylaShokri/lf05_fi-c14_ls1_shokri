import java.util.Scanner;

public class Monate {
	
	//wCmM6EKtN6zByk85YUc5
	//Aufgabe 2 (Monate)

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		int monat = scan.nextInt();
		
		switch(monat) {
		case 1: printSomething("Januar"); break;
		case 2: printSomething("Februar"); break;
		case 3: printSomething("Maerz"); break;
		case 4: printSomething("April"); break;
		case 5: printSomething("Mai"); break;
		case 6: printSomething("Juni"); break;
		case 7: printSomething("Juli"); break;
		case 8: printSomething("August"); break;
		case 9: printSomething("September"); break;
		case 10: printSomething("Oktober"); break;
		case 11: printSomething("November"); break;
		case 12: printSomething("Dezember"); break;
		}
	}
	
	static void printSomething(String s) {
		System.out.println(s);
	}

}
