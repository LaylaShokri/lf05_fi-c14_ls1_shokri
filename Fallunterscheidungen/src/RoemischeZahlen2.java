import java.util.Scanner;

public class RoemischeZahlen2 {

	//wCmM6EKtN6zByk85YUc5

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Eingabe: ");
		String input = scan.nextLine();
		String order = "IVXLCDM";

		String[] splitArray = input.toUpperCase().split("");

		int ergebnis = 0;

		// counter for repetition rule
		int c = 1;
		
		for(int i = 1; i < splitArray.length; i++) {
			int prev = romToInt(splitArray[i - 1]);
			String prevRomChar = splitArray[i - 1];
			int x = romToInt(splitArray[i]);
			String xRomChar = splitArray[i];

			
			// max repetition of numbers = 3
			if(x == prev) {
				c++; 
			}
			if(c >= 3) {
				System.out.println("Maximum an gleichen aufeinanderfolgenden Zeichen überschritten");
				System.exit(0);
			}
			
			// addidtion and subtraction rule

			if(x > prev) {

				// additional subtraction rule (rule 4)
				if(order.indexOf(xRomChar) - order.indexOf(prevRomChar) <= 2) {
					ergebnis -= prev;
				}
				else {
					System.out.println("Ungültige Eingabe");
					System.exit(0);
				}
			}
			else {
				ergebnis += prev;
			}
			if(i == splitArray.length - 1) {
				ergebnis += x;
			}
		}

		if(ergebnis != 0)
			System.out.println(ergebnis);
	}
	
	public static int romToInt(String rom) {
		
		int a = 0;
		
		switch(rom) {
		case "I": a=1; break;
		case "V": a=5; break;
		case "X": a=10; break;
		case "L": a=50; break;
		case "C": a=100; break;
		case "D": a=500; break;
		case "M": a=1000; break;
		default: System.out.println("Ungültige Eingabe");
		
		}
		return a;

	}
}
