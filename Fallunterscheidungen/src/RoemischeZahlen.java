import java.util.Scanner;

public class RoemischeZahlen {
	
	//wCmM6EKtN6zByk85YUc5
	// Aufgabe 3 (r�mische Zahlen)

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String zeichen = scan.nextLine();
		
		switch(zeichen) {
		case "I": System.out.println("1"); break;
		case "V": System.out.println("5"); break;
		case "X": System.out.println("10"); break;
		case "L": System.out.println("50"); break;
		case "C": System.out.println("100"); break;
		case "D": System.out.println("500"); break;
		case "M": System.out.println("1"); break;
		default: System.out.println("Ung�ltige Eingabe");
		}

	}

}
